plugins {
    kotlin("multiplatform") version "1.6.20-RC2"
    id("maven-publish")
}

group = "xyz.raspberryjam"
version = "0.2"

repositories {
    mavenCentral()
}

kotlin {
    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        val main by compilations.getting
        val cglm by main.cinterops.creating
    }
    
    sourceSets {
        val nativeMain by getting
        val nativeTest by getting
    }
}
