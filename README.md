# KGLM
KGLM is a Kotlin/Native wrapper library around the [CGLM](https://github.com/recp/cglm) library, written for C. CGLM is a highly optimised graphics mathematics library, and KGLM aims to present a Kotlin-style interface to it, while simplifying many of its operations using features not available to the C language.

### Type Safety
When using CGLM, vectors and matrices are usually just aliases for pointers to arrays. This can make it easier for a programmer to make a mistake, passing an incorrectly-sized vector or matrix to an inappropriate function, causing out-of-bounds operation and potentially undefined behaviour.

With KGLM, this can be avoided using Kotlin's far more robust type system. Vectors and matrices are defined as their own objects, and functions that operate using them will not accept objects that they are not supposed to.

### Operator Overloads
Using Kotlin's operator overloading feature, performing arithmetic using vector and matrix types can be written in a far more intuitive manner, much closer to typical mathematical form. It also allows stringing together multiple operations in a much more brief and concise format.

## Examples
Adding four vectors together in C with CGLM:
```c
vec3 v1 = { 1.0f, 2.0f, 4.0f };
vec3 v2 = { 5.0f, 2.0f, 1.0f };
vec3 v3 = { 7.6f, 3.0f, 12.4f };
vec3 v4 = { 9.0f, 5.1f, 9.0f };
vec3 result;
glm_vec3_add(v1, v2, result);
glm_vec3_add(result, v3, result);
glm_vec3_add(result, v4, result); // Result: { }
```

Same operation in Kotlin with KGLM:
```kotlin
val v1 = Vec3(1f, 2f, 4f)
val v2 = Vec3(5f, 2f, 1f)
val v3 = Vec3(7.6f, 3f, 12.4f)
val v4 = Vec3(9f, 5.1f, 9f)
val result = v1 + v2 + v3 + v4
```

Adding a vector to another in C with CGLM:
```c
vec3 myVec = { 1.0f, 2.0f, 3.0f };
// Another vec3 must be instantiated
vec3 otherVec = { 3.0f, 2.0f, 1.0f };
glm_vec3_add(myVec, otherVec, myVec); // myVec = { 4.0f, 4.0f, 4.0f }
```

Same operation in Kotlin with KGLM:
```kotlin
var myVec = Vec3(1f, 2f, 3f) // Second Vec3 can be instantiated in-line
myVec += Vec3(3f, 2f, 1f) // myVec = <4f, 4f, 4f>
```
