import xyz.raspberryjam.kglm.Vec2
import xyz.raspberryjam.kglm.Vec3
import kotlin.math.sqrt
import kotlin.test.Test
import kotlin.test.assertEquals

//region Vec2
@Test
fun createVec2() {
    val v = Vec2(1f, 3f)
    assertEquals(1f, v.x)
    assertEquals(3f, v.y)
}

//region Operator Overloads
//region Scalar
@Test
fun plusScalarVec2() {
    val v = Vec2(13f, 2f)
    val s = 4f
    val result = v + s
    assertEquals(13f + 4f, result.x)
    assertEquals(2f + 4f, result.y)
}

@Test
fun plusAssignScalarVec2() {
    val v = Vec2(69f, 420f)
    v += 10f
    assertEquals(69f + 10f, v.x)
    assertEquals(420f + 10f, v.y)
}

@Test
fun minusScalarVec2() {
    val v = Vec2(13f, 58f)
    val s = 22f
    val result = v - s
    assertEquals(13f - 22f, result.x)
    assertEquals(58f - 22f, result.y)
}

@Test
fun minusAssignScalarVec2() {
    val v = Vec2(45f, 2f)
    v -= 43f
    assertEquals(45f - 43f, v.x)
    assertEquals(2f - 43f, v.y)
}

@Test
fun timesScalarVec2() {
    val v = Vec2(33f, 22f)
    val s = 9f
    val result = v * s
    assertEquals(33f * 9f, result.x)
    assertEquals(22f * 9f, result.y)
}

@Test
fun timesAssignScalarVec2() {
    val v = Vec2(35f, 11f)
    v *= 17f
    assertEquals(35f * 17f, v.x)
    assertEquals(11f * 17f, v.y)
}

@Test
fun divScalarVec2() {
    val v = Vec2(66f, 55f)
    val s = 88f
    val result = v / s
    assertEquals(66f / 88f, result.x)
    assertEquals(55f / 88f, result.y)
}

@Test
fun divAssignScalarVec2() {
    val v = Vec2(11f, 22f)
    v /= 100f
    assertEquals(11f / 100f, v.x)
    assertEquals(22f / 100f, v.y)
}
//endregion
//region Vec2 rhs
@Test
fun plusVec2() {
    val v1 = Vec2(13f, 2f)
    val v2 = Vec2(4f, 12f)
    val result = v1 + v2
    assertEquals(13f + 4f, result.x)
    assertEquals(2f + 12f, result.y)
}

@Test
fun plusAssignVec2() {
    val v = Vec2(69f, 420f)
    v += Vec2(420f, 69f)
    assertEquals(69f + 420f, v.x)
    assertEquals(420f + 69f, v.y)
}

@Test
fun minusVec2() {
    val v1 = Vec2(13f, 58f)
    val v2 = Vec2(67f, 15f)
    val result = v1 - v2
    assertEquals(13f - 67f, result.x)
    assertEquals(58f - 15f, result.y)
}

@Test
fun minusAssignVec2() {
    val v = Vec2(45f, 2f)
    v -= Vec2(678f, 12f)
    assertEquals(45f - 678f, v.x)
    assertEquals(2f - 12f, v.y)
}

@Test
fun timesVec2() {
    val v1 = Vec2(33f, 22f)
    val v2 = Vec2(22f, 33f)
    val result = v1 * v2
    assertEquals(33f * 22f, result.x)
    assertEquals(22f * 33f, result.y)
}

@Test
fun timesAssignVec2() {
    val v = Vec2(35f, 11f)
    v *= Vec2(45f, 12f)
    assertEquals(35f * 45f, v.x)
    assertEquals(11f * 12f, v.y)
}

@Test
fun divVec2() {
    val v1 = Vec2(66f, 55f)
    val v2 = Vec2(44f, 33f)
    val result = v1 / v2
    assertEquals(66f / 44f, result.x)
    assertEquals(55f / 33f, result.y)
}

@Test
fun divAssignVec2() {
    val v = Vec2(11f, 22f)
    v /= Vec2(33f, 44f)
    assertEquals(11f / 33f, v.x)
    assertEquals(22f / 44f, v.y)
}
//endregion
//endregion

//region Methods
@Test
fun dotVec2() {
    val v1 = Vec2(22f, 33f)
    val v2 = Vec2(44f, 55f)
    val dot = v1.dot(v2)
    assertEquals((22f*44f)+(33f*55f), dot)
}
// Based on: https://allenchou.net/2013/07/cross-product-of-2d-vectors/
@Test
fun crossVec2() {
    val v1 = Vec2(11f, 22f)
    val v2 = Vec2(22f, 33f)
    val cross = v1.cross(v2)
    assertEquals((11f*33f) - (22f*22f), cross)
}
@Test
fun magnitudeVec2() {
    val v = Vec2(48f, 20f)
    val magnitude = v.magnitude()
    assertEquals(sqrt((48f*48f) + (20f*20f)), magnitude)
}
@Test
fun squMagnitudeVec2() {
    val v = Vec2(34f, 12f)
    val squMagnitude = v.squMagnitude()
    assertEquals((34f*34f) + (12f*12f), squMagnitude)
}
@Test
fun negateVec2() {
    val v = Vec2(55f, 66f)
    val result = v.negate()
    assertEquals(-55f, result.x)
    assertEquals(-66f, result.y)
}
@Test
fun normalizeVec2() {
    val v = Vec2(29f, 30f)
    val result = v.normalize()
    val magnitude = sqrt((29f*29f) + (30f*30f))
    assertEquals(29f / magnitude, result.x)
    assertEquals(30f / magnitude, result.y)
}
@Test
fun distanceVec2() {
    val v1 = Vec2(12f, 27f)
    val v2 = Vec2(30f, 15f)
    val result = v1.distance(v2)
    assertEquals(sqrt((18f*18f)+(12f*12f)), result)
}
@Test
fun maxVec2() {
    val v1 = Vec2(6f, 10f)
    val v2 = Vec2(12f, 2f)
    val result = v1.max(v2)
    assertEquals(Vec2(12f, 10f), result)
}
@Test
fun minVec2() {
    val v1 = Vec2(6f, 10f)
    val v2 = Vec2(12f, 2f)
    val result = v1.min(v2)
    assertEquals(Vec2(6f, 2f), result)
}
// TODO: Wtf does clamp even do
@Test
fun lerpVec2() {
    val v1 = Vec2(18f, 5f)
    val v2 = Vec2(12f, 20f)
    val step = 0.5f
    val result = v1.lerp(v2, step)
    assertEquals(15f, result.x)
    assertEquals(12.5f, result.y)
}
@Test
fun zeroVec2() {
    val v = Vec2.zero()
    assertEquals(Vec2(0f, 0f), v)
}
@Test
fun oneVec2() {
    val v = Vec2.one()
    assertEquals(Vec2(1f, 1f), v)
}
//endregion

//endregion

//region Vec3
@Test
fun createVec3() {
    val v = Vec3(1f, 3f, 4f)
    assertEquals(1f, v.x)
    assertEquals(3f, v.y)
    assertEquals(4f, v.z)
}

//region Operator Overloads
//region Scalar
@Test
fun plusScalarVec3() {
    val v = Vec3(13f, 2f, 5f)
    val s = 4f
    val result = v + s
    assertEquals(13f + 4f, result.x)
    assertEquals(2f + 4f, result.y)
    assertEquals(5f + 4f, result.z)
}

@Test
fun plusAssignScalarVec3() {
    val v = Vec3(69f, 420f, 2f)
    v += 10f
    assertEquals(69f + 10f, v.x)
    assertEquals(420f + 10f, v.y)
    assertEquals(2f + 10f, v.z)
}

@Test
fun minusScalarVec3() {
    val v = Vec3(13f, 58f, 6f)
    val s = 22f
    val result = v - s
    assertEquals(13f - 22f, result.x)
    assertEquals(58f - 22f, result.y)
    assertEquals(6f - 22f, result.z)
}

@Test
fun minusAssignScalarVec3() {
    val v = Vec3(45f, 2f, 9f)
    v -= 43f
    assertEquals(45f - 43f, v.x)
    assertEquals(2f - 43f, v.y)
    assertEquals(9f - 43f, v.z)
}

@Test
fun timesScalarVec3() {
    val v = Vec3(33f, 22f, 11f)
    val s = 9f
    val result = v * s
    assertEquals(33f * 9f, result.x)
    assertEquals(22f * 9f, result.y)
    assertEquals(11f * 9f, result.z)
}

@Test
fun timesAssignScalarVec3() {
    val v = Vec3(35f, 11f, 13f)
    v *= 17f
    assertEquals(35f * 17f, v.x)
    assertEquals(11f * 17f, v.y)
    assertEquals(13f * 17f, v.z)
}

@Test
fun divScalarVec3() {
    val v = Vec3(66f, 55f, 44f)
    val s = 88f
    val result = v / s
    assertEquals(66f / 88f, result.x)
    assertEquals(55f / 88f, result.y)
    assertEquals(44f / 88f, result.z)
}

@Test
fun divAssignScalarVec3() {
    val v = Vec3(11f, 22f, 33f)
    v /= 100f
    assertEquals(11f / 100f, v.x)
    assertEquals(22f / 100f, v.y)
    assertEquals(33f / 100f, v.z)
}
//endregion
//region Vec2 rhs
@Test
fun plusVec3() {
    val v1 = Vec3(13f, 2f, 6f)
    val v2 = Vec3(4f, 12f, 7f)
    val result = v1 + v2
    assertEquals(13f + 4f, result.x)
    assertEquals(2f + 12f, result.y)
    assertEquals(6f + 7f, result.z)
}

@Test
fun plusAssignVec3() {
    val v = Vec3(69f, 420f, 20f)
    v += Vec3(420f, 69f, 20f)
    assertEquals(69f + 420f, v.x)
    assertEquals(420f + 69f, v.y)
    assertEquals(20f + 20f, v.z)
}

@Test
fun minusVec3() {
    val v1 = Vec3(13f, 58f, 59f)
    val v2 = Vec3(67f, 15f, 46f)
    val result = v1 - v2
    assertEquals(13f - 67f, result.x)
    assertEquals(58f - 15f, result.y)
    assertEquals(59f - 46f, result.z)
}

@Test
fun minusAssignVec3() {
    val v = Vec3(45f, 2f, 9f)
    v -= Vec3(678f, 12f, 12f)
    assertEquals(45f - 678f, v.x)
    assertEquals(2f - 12f, v.y)
    assertEquals(9f - 12f, v.z)
}

@Test
fun timesVec3() {
    val v1 = Vec3(33f, 22f, 11f)
    val v2 = Vec3(22f, 33f, 44f)
    val result = v1 * v2
    assertEquals(33f * 22f, result.x)
    assertEquals(22f * 33f, result.y)
    assertEquals(11f * 44f, result.z)
}

@Test
fun timesAssignVec3() {
    val v = Vec3(35f, 11f, 44f)
    v *= Vec3(45f, 12f, 39f)
    assertEquals(35f * 45f, v.x)
    assertEquals(11f * 12f, v.y)
    assertEquals(44f, 39f, v.z)
}

@Test
fun divVec3() {
    val v1 = Vec3(66f, 55f, 77f)
    val v2 = Vec3(44f, 33f, 22f)
    val result = v1 / v2
    assertEquals(66f / 44f, result.x)
    assertEquals(55f / 33f, result.y)
    assertEquals(77f / 22f, result.z)
}

@Test
fun divAssignVec3() {
    val v = Vec3(11f, 22f, 33f)
    v /= Vec3(33f, 44f, 55f)
    assertEquals(11f / 33f, v.x)
    assertEquals(22f / 44f, v.y)
    assertEquals(33f / 55f, v.z)
}
//endregion
//endregion

//region Methods
@Test
fun dotVec3() {
    val v1 = Vec3(22f, 33f, 44f)
    val v2 = Vec3(44f, 55f, 66f)
    val dot = v1.dot(v2)
    assertEquals((22f*44f)+(33f*55f)+(44f*66f), dot)
}
@Test
fun crossVec3() {
    val v1 = Vec3(11f, 22f, 33f)
    val v2 = Vec3(22f, 33f, 44f)
    val cross = v1.cross(v2)
    assertEquals(22f*44f-33f*33f, cross.x)
    assertEquals(33f*22f-11f*44f, cross.y)
    assertEquals(11f*33f-22f*22f, cross.z)
}
@Test
fun magnitudeVec3() {
    val v = Vec3(48f, 20f, 77f)
    val magnitude = v.magnitude()
    assertEquals(sqrt((48f*48f) + (20f*20f) + (77f*77f)), magnitude)
}
@Test
fun squMagnitudeVec3() {
    val v = Vec3(34f, 12f, 50f)
    val squMagnitude = v.squMagnitude()
    assertEquals((34f*34f) + (12f*12f) + (50f*50f), squMagnitude)
}
@Test
fun negateVec3() {
    val v = Vec3(55f, 66f, 77f)
    val result = v.negate()
    assertEquals(-55f, result.x)
    assertEquals(-66f, result.y)
    assertEquals(-77f, result.z)
}
@Test
fun normalizeVec3() {
    val v = Vec3(29f, 30f, 31f)
    val result = v.normalize()
    val magnitude = sqrt((29f*29f) + (30f*30f) + (31f*31f))
    assertEquals(29f / magnitude, result.x)
    assertEquals(30f / magnitude, result.y)
    assertEquals(31f / magnitude, result.z)
}
@Test
fun distanceVec3() {
    val v1 = Vec3(12f, 27f, 39f)
    val v2 = Vec3(30f, 15f, 34f)
    val result = v1.distance(v2)
    assertEquals(sqrt((18f*18f)+(12f*12f)+(5f*5f)), result)
}
@Test
fun maxVec3() {
    val v1 = Vec3(6f, 10f, 14f)
    val v2 = Vec3(12f, 2f, -8f)
    val result = v1.max(v2)
    assertEquals(Vec3(12f, 10f, 14f), result)
}
@Test
fun minVec3() {
    val v1 = Vec3(6f, 10f, 14f)
    val v2 = Vec3(12f, 2f, -8f)
    val result = v1.min(v2)
    assertEquals(Vec3(6f, 2f, -8f), result)
}
// TODO: Wtf does clamp even do
@Test
fun lerpVec3() {
    val v1 = Vec3(18f, 5f, 9f)
    val v2 = Vec3(12f, 20f, 12f)
    val step = 0.5f
    val result = v1.lerp(v2, step)
    assertEquals(15f, result.x)
    assertEquals(12.5f, result.y)
    assertEquals(10.5f, result.z)
}
@Test
fun zeroVec3() {
    val v = Vec3.zero()
    assertEquals(Vec3(0f, 0f, 0f), v)
}
@Test
fun oneVec3() {
    val v = Vec3.one()
    assertEquals(Vec3(1f, 1f, 1f), v)
}
//endregion

//endregion