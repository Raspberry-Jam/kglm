package xyz.raspberryjam.kglm

import cglm.*
import kotlinx.cinterop.*

internal fun Pinned<FloatArray>.ptr(): CPointer<FloatVar> = this.addressOf(0)

/**
 * A vector with 2 components
 */
class Vec2 {
    //region Class Members
    // The pointer that kotlin can pass to C and read values from
    internal val _vec2 = FloatArray(2).pin()

    /**
     * X component
     */
    var x: Float
        get() = _vec2.get()[0] // Retrieve from the C-compatible array
        set(value) {
            _vec2.get()[0] = value // Set in the C-compatible array
        }

    /**
     * Y component
     */
    var y: Float
        get() = _vec2.get()[1] // Retrieve from the C-compatible array
        set(value) {
            _vec2.get()[1] = value // Set in the C-compatible
        }
    //endregion

    //region Constructor
    constructor(x: Float, y: Float) {
        this.x = x
        this.y = y
    }
    //endregion

    //region Operator Overloads
    //region Vec2
    operator fun plus(rhs: Vec2): Vec2 {
        val dest = zero()
        glm_vec2_add(this._vec2.addressOf(0), rhs._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    operator fun plusAssign(rhs: Vec2) {
        glm_vec2_add(this._vec2.addressOf(0), rhs._vec2.ptr(), this._vec2.ptr()) // Directly output result back to self
    }
    operator fun minus(rhs: Vec2): Vec2 {
        val dest = zero()
        glm_vec2_sub(this._vec2.ptr(), rhs._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    operator fun minusAssign(rhs: Vec2) {
        glm_vec2_sub(this._vec2.ptr(), rhs._vec2.ptr(), this._vec2.ptr()) // Directly output result back to self
    }
    operator fun times(rhs: Vec2): Vec2 {
        val dest = zero()
        glm_vec2_mul(this._vec2.ptr(), rhs._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    operator fun timesAssign(rhs: Vec2) {
        glm_vec2_mul(this._vec2.ptr(), rhs._vec2.ptr(), this._vec2.ptr()) // Directly output result back to self
    }
    operator fun div(rhs: Vec2): Vec2 {
        val dest = zero()
        glm_vec2_div(this._vec2.ptr(), rhs._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    operator fun divAssign(rhs: Vec2) {
        glm_vec2_div(this._vec2.ptr(), rhs._vec2.ptr(), this._vec2.ptr()) // Directly output result back to self
    }
    override operator fun equals(other: Any?): Boolean {
        if (other !is Vec2) return false
        return this.x == other.x && this.y == other.y
    }
    //endregion
    //region Scalar
    operator fun plus(rhs: Float): Vec2 {
        val dest = zero()
        glm_vec2_adds(this._vec2.ptr(), rhs, dest._vec2.ptr())
        return dest
    }
    operator fun plusAssign(rhs: Float) {
        glm_vec2_adds(this._vec2.ptr(), rhs, this._vec2.ptr())
    }
    operator fun minus(rhs: Float): Vec2 {
        val dest = zero()
        glm_vec2_subs(this._vec2.ptr(), rhs, dest._vec2.ptr())
        return dest
    }
    operator fun minusAssign(rhs: Float) {
        glm_vec2_subs(this._vec2.ptr(), rhs, this._vec2.ptr())
    }
    operator fun times(rhs: Float): Vec2 {
        val dest = zero()
        glm_vec2_scale(this._vec2.ptr(), rhs, dest._vec2.ptr())
        return dest
    }
    operator fun timesAssign(rhs: Float) {
        glm_vec2_scale(this._vec2.ptr(), rhs, this._vec2.ptr())
    }
    operator fun div(rhs: Float): Vec2 {
        val dest = zero()
        glm_vec2_divs(this._vec2.ptr(), rhs, dest._vec2.ptr())
        return dest
    }
    operator fun divAssign(rhs: Float) {
        glm_vec2_divs(this._vec2.ptr(), rhs, this._vec2.ptr())
    }
    //endregion
    //endregion

    //region Methods
    fun dot(rhs: Vec2): Float {
        return glm_vec2_dot(this._vec2.ptr(), rhs._vec2.ptr())
    }
    fun cross(rhs: Vec2): Float {
        return glm_vec2_cross(this._vec2.ptr(), rhs._vec2.ptr())
    }
    fun magnitude(): Float {
        return glm_vec2_norm(this._vec2.ptr())
    }
    fun squMagnitude(): Float {
        return glm_vec2_norm2(this._vec2.ptr())
    }
    fun negate(): Vec2 {
        val dest = zero()
        glm_vec2_negate_to(this._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    fun normalize(): Vec2 {
        val dest = zero()
        glm_vec2_normalize_to(this._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    fun distance(other: Vec2): Float {
        return glm_vec2_distance(this._vec2.ptr(), other._vec2.ptr())
    }
    fun max(other: Vec2): Vec2 {
        val dest = zero()
        glm_vec2_maxv(this._vec2.ptr(), other._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    fun min(other: Vec2): Vec2 {
        val dest = zero()
        glm_vec2_minv(this._vec2.ptr(), other._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    fun clamp(min: Float, max: Float): Vec2 {
        val dest = this
        glm_vec2_clamp(dest._vec2.ptr(), min, max)
        return dest
    }
    fun lerp(to: Vec2, step: Float): Vec2 {
        val dest = zero()
        glm_vec2_lerp(this._vec2.ptr(), to._vec2.ptr(), step, dest._vec2.ptr())
        return dest
    }
    //endregion

    companion object {
        fun zero(): Vec2 {
            return Vec2(0f, 0f)
        }
        fun one(): Vec2 {
            return Vec2(1f, 1f)
        }
    }

    private fun finalize() {
        _vec2.unpin() // Unpin the array, allowing it to be collected by the garbage collector
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }
}

/**
 * A vector with 3 components
 */
class Vec3 {
    //region Class Members
    // The pointer that kotlin can pass to C and read values from
    internal val _vec3 = FloatArray(3).pin()

    /**
     * X component
     */
    var x: Float
        get() = _vec3.get()[0] // Retrieve from the C-compatible array
        set(value) {
            _vec3.get()[0] = value // Set in the C-compatible array
        }

    /**
     * Y component
     */
    var y: Float
        get() = _vec3.get()[1] // Retrieve from the C-compatible array
        set(value) {
            _vec3.get()[1] = value // Set in the C-compatible
        }

    var z: Float
        get() = _vec3.get()[2]
        set(value) {
            _vec3.get()[2] = value
        }
    //endregion

    //region Constructor
    constructor(x: Float, y: Float, z: Float) {
        this.x = x
        this.y = y
        this.z = z
    }
    //endregion

    //region Operator Overloads
    //region Vec3
    operator fun plus(rhs: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_add(this._vec3.addressOf(0), rhs._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    operator fun plusAssign(rhs: Vec3) {
        glm_vec3_add(this._vec3.addressOf(0), rhs._vec3.ptr(), this._vec3.ptr()) // Directly output result back to self
    }
    operator fun minus(rhs: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_sub(this._vec3.ptr(), rhs._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    operator fun minusAssign(rhs: Vec3) {
        glm_vec3_sub(this._vec3.ptr(), rhs._vec3.ptr(), this._vec3.ptr()) // Directly output result back to self
    }
    operator fun times(rhs: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_mul(this._vec3.ptr(), rhs._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    operator fun timesAssign(rhs: Vec3) {
        glm_vec3_mul(this._vec3.ptr(), rhs._vec3.ptr(), this._vec3.ptr()) // Directly output result back to self
    }
    operator fun div(rhs: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_div(this._vec3.ptr(), rhs._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    operator fun divAssign(rhs: Vec3) {
        glm_vec3_div(this._vec3.ptr(), rhs._vec3.ptr(), this._vec3.ptr()) // Directly output result back to self
    }
    override operator fun equals(other: Any?): Boolean {
        if (other !is Vec3) return false
        return this.x == other.x && this.y == other.y && this.z == other.z
    }
    //endregion
    //region Scalar
    operator fun plus(rhs: Float): Vec3 {
        val dest = zero()
        glm_vec3_adds(this._vec3.ptr(), rhs, dest._vec3.ptr())
        return dest
    }
    operator fun plusAssign(rhs: Float) {
        glm_vec3_adds(this._vec3.ptr(), rhs, this._vec3.ptr())
    }
    operator fun minus(rhs: Float): Vec3 {
        val dest = zero()
        glm_vec3_subs(this._vec3.ptr(), rhs, dest._vec3.ptr())
        return dest
    }
    operator fun minusAssign(rhs: Float) {
        glm_vec3_subs(this._vec3.ptr(), rhs, this._vec3.ptr())
    }
    operator fun times(rhs: Float): Vec3 {
        val dest = zero()
        glm_vec3_scale(this._vec3.ptr(), rhs, dest._vec3.ptr())
        return dest
    }
    operator fun timesAssign(rhs: Float) {
        glm_vec3_scale(this._vec3.ptr(), rhs, this._vec3.ptr())
    }
    operator fun div(rhs: Float): Vec3 {
        val dest = zero()
        glm_vec3_divs(this._vec3.ptr(), rhs, dest._vec3.ptr())
        return dest
    }
    operator fun divAssign(rhs: Float) {
        glm_vec3_divs(this._vec3.ptr(), rhs, this._vec3.ptr())
    }
    //endregion
    //endregion

    //region Methods
    fun dot(rhs: Vec3): Float {
        return glm_vec3_dot(this._vec3.ptr(), rhs._vec3.ptr())
    }
    fun cross(other: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_cross(this._vec3.ptr(), other._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    fun magnitude(): Float {
        return glm_vec3_norm(this._vec3.ptr())
    }
    fun squMagnitude(): Float {
        return glm_vec3_norm2(this._vec3.ptr())
    }
    fun negate(): Vec3 {
        val dest = zero()
        glm_vec3_negate_to(this._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    fun normalize(): Vec3 {
        val dest = zero()
        glm_vec3_normalize_to(this._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    fun distance(other: Vec3): Float {
        return glm_vec3_distance(this._vec3.ptr(), other._vec3.ptr())
    }
    fun max(other: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_maxv(this._vec3.ptr(), other._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    fun min(other: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_minv(this._vec3.ptr(), other._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    fun clamp(min: Float, max: Float) {
        glm_vec3_clamp(this._vec3.ptr(), min, max)
    }
    fun lerp(to: Vec3, step: Float): Vec3 {
        val dest = zero()
        glm_vec3_lerp(this._vec3.ptr(), to._vec3.ptr(), step, dest._vec3.ptr())
        return dest
    }
    fun rotate(axis: Vec3, angle: Float) {
        glm_vec3_rotate(this._vec3.ptr(), angle, axis._vec3.ptr())
    }
    fun rotateM3(matrix: Mat3) {
        glm_vec3_rotate_m3(matrix._mat3.ptr(), this._vec3.ptr(), this._vec3.ptr())
    }
    fun rotateM4(matrix: Mat4) {
        glm_vec3_rotate_m4(matrix._mat4.ptr(), this._vec3.ptr(), this._vec3.ptr())
    }
    fun proj(onto: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_proj(this._vec3.ptr(), onto._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    fun center(other: Vec3): Vec3 {
        val dest = zero()
        glm_vec3_center(this._vec3.ptr(), other._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    fun ortho(): Vec3 {
        val dest = zero()
        glm_vec3_ortho(this._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    //endregion

    companion object {
        fun zero(): Vec3 {
            return Vec3(0f, 0f, 0f)
        }
        fun one(): Vec3 {
            return Vec3(1f, 1f, 1f)
        }
    }

    private fun finalize() {
        _vec3.unpin() // Unpin the array, allowing it to be collected by the garbage collector
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + z.hashCode()
        return result
    }
}

/**
 * A vector with 4 components
 */
class Vec4 {
    //region Class Members
    // The pointer that kotlin can pass to C and read values from
    internal val _vec4 = FloatArray(4).pin()

    /**
     * X component
     */
    var x: Float
        get() = _vec4.get()[0] // Retrieve from the C-compatible array
        set(value) {
            _vec4.get()[0] = value // Set in the C-compatible array
        }

    /**
     * Y component
     */
    var y: Float
        get() = _vec4.get()[1] // Retrieve from the C-compatible array
        set(value) {
            _vec4.get()[1] = value // Set in the C-compatible
        }

    var z: Float
        get() = _vec4.get()[2]
        set(value) {
            _vec4.get()[2] = value
        }
    
    var w: Float
        get() = _vec4.get()[3]
        set(value) {
            _vec4.get()[3] = value
        }
    //endregion

    //region Constructor
    constructor(x: Float, y: Float, z: Float, w: Float) {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
    }
    //endregion

    //region Operator Overloads
    //region Vec4
    operator fun plus(rhs: Vec4): Vec4 {
        val dest = zero()
        glm_vec4_add(this._vec4.addressOf(0), rhs._vec4.ptr(), dest._vec4.ptr())
        return dest
    }
    operator fun plusAssign(rhs: Vec4) {
        glm_vec4_add(this._vec4.addressOf(0), rhs._vec4.ptr(), this._vec4.ptr()) // Directly output result back to self
    }
    operator fun minus(rhs: Vec4): Vec4 {
        val dest = zero()
        glm_vec4_sub(this._vec4.ptr(), rhs._vec4.ptr(), dest._vec4.ptr())
        return dest
    }
    operator fun minusAssign(rhs: Vec4) {
        glm_vec4_sub(this._vec4.ptr(), rhs._vec4.ptr(), this._vec4.ptr()) // Directly output result back to self
    }
    operator fun times(rhs: Vec4): Vec4 {
        val dest = zero()
        glm_vec4_mul(this._vec4.ptr(), rhs._vec4.ptr(), dest._vec4.ptr())
        return dest
    }
    operator fun timesAssign(rhs: Vec4) {
        glm_vec4_mul(this._vec4.ptr(), rhs._vec4.ptr(), this._vec4.ptr()) // Directly output result back to self
    }
    operator fun div(rhs: Vec4): Vec4 {
        val dest = zero()
        glm_vec4_div(this._vec4.ptr(), rhs._vec4.ptr(), dest._vec4.ptr())
        return dest
    }
    operator fun divAssign(rhs: Vec4) {
        glm_vec4_div(this._vec4.ptr(), rhs._vec4.ptr(), this._vec4.ptr()) // Directly output result back to self
    }
    override operator fun equals(other: Any?): Boolean {
        if (other !is Vec4) return false
        return this.x == other.x && this.y == other.y && this.z == other.z && this.w == other.w
    }
    //endregion
    //region Scalar
    operator fun plus(rhs: Float): Vec4 {
        val dest = zero()
        glm_vec4_adds(this._vec4.ptr(), rhs, dest._vec4.ptr())
        return dest
    }
    operator fun plusAssign(rhs: Float) {
        glm_vec4_adds(this._vec4.ptr(), rhs, this._vec4.ptr())
    }
    operator fun minus(rhs: Float): Vec4 {
        val dest = zero()
        glm_vec4_subs(this._vec4.ptr(), rhs, dest._vec4.ptr())
        return dest
    }
    operator fun minusAssign(rhs: Float) {
        glm_vec4_subs(this._vec4.ptr(), rhs, this._vec4.ptr())
    }
    operator fun times(rhs: Float): Vec4 {
        val dest = zero()
        glm_vec4_scale(this._vec4.ptr(), rhs, dest._vec4.ptr())
        return dest
    }
    operator fun timesAssign(rhs: Float) {
        glm_vec4_scale(this._vec4.ptr(), rhs, this._vec4.ptr())
    }
    operator fun div(rhs: Float): Vec4 {
        val dest = zero()
        glm_vec4_divs(this._vec4.ptr(), rhs, dest._vec4.ptr())
        return dest
    }
    operator fun divAssign(rhs: Float) {
        glm_vec4_divs(this._vec4.ptr(), rhs, this._vec4.ptr())
    }
    //endregion
    //endregion

    //region Methods
    fun dot(rhs: Vec4): Float {
        return glm_vec4_dot(this._vec4.ptr(), rhs._vec4.ptr())
    }
    fun magnitude(): Float {
        return glm_vec4_norm(this._vec4.ptr())
    }
    fun squMagnitude(): Float {
        return glm_vec4_norm2(this._vec4.ptr())
    }
    fun negate() {
        glm_vec4_negate(this._vec4.ptr())
    }
    fun normalize() {
        glm_vec4_normalize(this._vec4.ptr())
    }
    fun distance(other: Vec4): Float {
        return glm_vec4_distance(this._vec4.ptr(), other._vec4.ptr())
    }
    fun max(other: Vec4): Vec4 {
        val dest = zero()
        glm_vec4_maxv(this._vec4.ptr(), other._vec4.ptr(), dest._vec4.ptr())
        return dest
    }
    fun min(other: Vec4): Vec4 {
        val dest = zero()
        glm_vec4_minv(this._vec4.ptr(), other._vec4.ptr(), dest._vec4.ptr())
        return dest
    }
    fun clamp(min: Float, max: Float) {
        glm_vec4_clamp(this._vec4.ptr(), min, max)
    }
    fun lerp(to: Vec4, step: Float): Vec4 {
        val dest = zero()
        glm_vec4_lerp(this._vec4.ptr(), to._vec4.ptr(), step, dest._vec4.ptr())
        return dest
    }
    //endregion

    companion object {
        fun cubic(side: Float): Vec4 {
            val dest = zero()
            glm_vec4_cubic(side, dest._vec4.ptr())
            return dest
        }
        fun zero(): Vec4 {
            return Vec4(0f, 0f, 0f, 0f)
        }
        fun one(): Vec4 {
            return Vec4(1f, 1f, 1f, 1f)
        }
    }

    private fun finalize() {
        _vec4.unpin() // Unpin the array, allowing it to be collected by the garbage collector
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + z.hashCode()
        result = 31 * result + w.hashCode()
        return result
    }
}

class InvalidSizeArray(message: String) : Exception(message)