package xyz.raspberryjam.kglm

import cglm.*
import kotlinx.cinterop.Pinned
import kotlinx.cinterop.pin

/**
 * A 2x2 matrix. Stored in Column-Major format in memory
 */
class Mat2 {
    //region Class Members
    internal val _mat2 = FloatArray(ROWS*COLUMNS).pin()
    //endregion

    //region Constructors
    constructor(
        m00: Float, m01: Float,
        m10: Float, m11: Float
    ) {
        this[0, 0] = m00; this[0, 1] = m01
        this[1, 0] = m10; this[1, 1] = m11
    }

    internal constructor(inPtr: Pinned<FloatArray>) {
        if (inPtr.get().size != ROWS*COLUMNS) throw InvalidSizeArray("Attempted to use array of size ${inPtr.get().size} for Mat2")
        val arr = inPtr.get()
        this[0, 0] = arr[0]; this[0, 1] = arr[2]
        this[1, 0] = arr[1]; this[1, 1] = arr[3]
        inPtr.unpin()
    }
    //endregion

    //region Operator Overloads
    operator fun get(row: Int, column: Int): Float {
        return _mat2.get()[(ROWS * column) + row]
    }
    operator fun set(row: Int, column: Int, value: Float) {
        this[row, column] = value
    }
    operator fun times(rhs: Mat2): Mat2 {
        val dest = zero()
        glm_mat2_mul(this._mat2.ptr(), rhs._mat2.ptr(), dest._mat2.ptr())
        return dest
    }
    operator fun times(rhs: Vec2): Vec2 {
        val dest = Vec2.zero()
        glm_mat2_mulv(this._mat2.ptr(), rhs._vec2.ptr(), dest._vec2.ptr())
        return dest
    }
    operator fun times(rhs: Float): Mat2 {
        val dest = copy(this)
        glm_mat2_scale(dest._mat2.ptr(), rhs)
        return dest
    }
    override operator fun equals(other: Any?): Boolean {
        if (other !is Mat2) return false
        return  this[0, 0] == other[0, 0] && this[1, 0] == other[1, 0] &&
                this[0, 1] == other[0, 1] && this[1, 1] == other[1, 1]
    }
    //endregion

    //region Methods
    fun transpose(): Mat2 {
        val dest = zero()
        glm_mat2_transpose_to(this._mat2.ptr(), dest._mat2.ptr())
        return dest
    }
    fun inverse(): Mat2 {
        val dest = zero()
        glm_mat2_inv(this._mat2.ptr(), dest._mat2.ptr())
        return dest
    }
    fun determinant(): Float {
        return glm_mat2_det(this._mat2.ptr())
    }
    fun trace(): Float {
        return glm_mat2_trace(this._mat2.ptr())
    }
    fun swapCol(a: Int, b: Int) {
        glm_mat2_swap_col(this._mat2.ptr(), a, b)
    }
    fun swapRow(a: Int, b: Int) {
        glm_mat2_swap_row(this._mat2.ptr(), a, b)
    }
    fun rmc(row: Vec2, column: Vec2): Float {
        return glm_mat2_rmc(row._vec2.ptr(), this._mat2.ptr(), column._vec2.ptr())
    }
    //endregion

    companion object {
        const val ROWS = 2
        const val COLUMNS = 2
        fun identity(): Mat2 {
            val dest = zero()
            glm_mat2_identity(dest._mat2.ptr())
            return dest
        }
        fun zero(): Mat2 {
            val dest = FloatArray(ROWS*COLUMNS).pin()
            glm_mat2_zero(dest.ptr())
            return Mat2(dest)
        }
        fun copy(source: Mat2): Mat2 {
            val dest = zero()
            glm_mat2_copy(source._mat2.ptr(), dest._mat2.ptr())
            return dest
        }
    }

    private fun finalize() {
        _mat2.unpin()
    }

    override fun hashCode(): Int {
        var result =           this[0, 0].hashCode()
        result = 31 * result + this[1, 0].hashCode()
        result = 31 * result + this[0, 1].hashCode()
        result = 31 * result + this[1, 1].hashCode()
        return result
    }
}

/**
 * A 3x3 matrix. Stored in Column-Major format in memory
 */
class Mat3 {
    //region Class Members
    internal val _mat3 = FloatArray(ROWS*COLUMNS).pin()
    //endregion

    //region Constructors
    constructor(
        m00: Float, m01: Float, m02: Float,
        m10: Float, m11: Float, m12: Float,
        m20: Float, m21: Float, m22: Float
    ) {
        this[0, 0] = m00; this[0, 1] = m01; this[0, 2] = m02
        this[1, 0] = m10; this[1, 1] = m11; this[1, 2] = m12
        this[2, 0] = m20; this[2, 1] = m21; this[2, 2] = m22
    }

    internal constructor(inPtr: Pinned<FloatArray>) {
        if (inPtr.get().size != ROWS*COLUMNS) throw InvalidSizeArray("Attempted to use array of size ${inPtr.get().size} for Mat2")
        val arr = inPtr.get()
        this[0, 0] = arr[0]; this[0, 1] = arr[3]; this[0, 2] = arr[6]
        this[1, 0] = arr[1]; this[1, 1] = arr[4]; this[1, 2] = arr[7]
        this[2, 0] = arr[2]; this[2, 1] = arr[5]; this[2, 2] = arr[8]
        inPtr.unpin()
    }
    //endregion

    //region Operator Overloads
    operator fun get(row: Int, column: Int): Float {
        return _mat3.get()[(ROWS * column) + row]
    }
    operator fun set(row: Int, column: Int, value: Float) {
        this[row, column] = value
    }
    operator fun times(rhs: Mat3): Mat3 {
        val dest = zero()
        glm_mat3_mul(this._mat3.ptr(), rhs._mat3.ptr(), dest._mat3.ptr())
        return dest
    }
    operator fun times(rhs: Vec3): Vec3 {
        val dest = Vec3.zero()
        glm_mat3_mulv(this._mat3.ptr(), rhs._vec3.ptr(), dest._vec3.ptr())
        return dest
    }
    operator fun times(rhs: Float): Mat3 {
        val dest = copy(this)
        glm_mat3_scale(dest._mat3.ptr(), rhs)
        return dest
    }
    override operator fun equals(other: Any?): Boolean {
        if (other !is Mat3) return false
        return  this[0, 0] == other[0, 0] && this[1, 0] == other[1, 0] && this[2, 0] == other[2, 0] &&
                this[0, 1] == other[0, 1] && this[1, 1] == other[1, 1] && this[2, 1] == other[2, 1] &&
                this[0, 2] == other[0, 2] && this[1, 2] == other[1, 2] && this[2, 2] == other[2, 2]
    }
    //endregion

    //region Methods
    fun transpose(): Mat3 {
        val dest = zero()
        glm_mat3_transpose_to(this._mat3.ptr(), dest._mat3.ptr())
        return dest
    }
    fun inverse(): Mat3 {
        val dest = zero()
        glm_mat3_inv(this._mat3.ptr(), dest._mat3.ptr())
        return dest
    }
    fun determinant(): Float {
        return glm_mat3_det(this._mat3.ptr())
    }
    fun trace(): Float {
        return glm_mat3_trace(this._mat3.ptr())
    }
    fun swapCol(a: Int, b: Int) {
        glm_mat3_swap_col(this._mat3.ptr(), a, b)
    }
    fun swapRow(a: Int, b: Int) {
        glm_mat3_swap_row(this._mat3.ptr(), a, b)
    }
    fun rmc(row: Vec3, column: Vec3): Float {
        return glm_mat3_rmc(row._vec3.ptr(), this._mat3.ptr(), column._vec3.ptr())
    }
    // TODO: Mat3 to Quaternion method (glm_mat3_quat)
    //endregion

    companion object {
        const val ROWS = 3
        const val COLUMNS = 3
        fun identity(): Mat3 {
            val dest = FloatArray(ROWS*COLUMNS).pin()
            glm_mat3_identity(dest.ptr())
            return Mat3(dest)
        }
        fun zero(): Mat3 {
            val dest = FloatArray(ROWS*COLUMNS).pin()
            glm_mat3_zero(dest.ptr())
            return Mat3(dest)
        }
        fun copy(source: Mat3): Mat3 {
            val dest = FloatArray(ROWS*COLUMNS).pin()
            glm_mat3_copy(source._mat3.ptr(), dest.ptr())
            return Mat3(dest)
        }
    }

    private fun finalize() {
        _mat3.unpin()
    }

    override fun hashCode(): Int {
        var result =           this[0, 0].hashCode()
        result = 31 * result + this[1, 0].hashCode()
        result = 31 * result + this[2, 0].hashCode()
        result = 31 * result + this[0, 1].hashCode()
        result = 31 * result + this[1, 1].hashCode()
        result = 31 * result + this[2, 1].hashCode()
        result = 31 * result + this[0, 2].hashCode()
        result = 31 * result + this[1, 2].hashCode()
        result = 31 * result + this[2, 2].hashCode()
        return result
    }
}

/**
 * A 4x4 matrix. Stored in Column-Major format in memory
 */
class Mat4 {
    //region Class Members
    internal val _mat4 = FloatArray(ROWS*COLUMNS).pin()
    //endregion

    //region Constructors
    constructor(
        m00: Float, m01: Float, m02: Float, m03: Float,
        m10: Float, m11: Float, m12: Float, m13: Float,
        m20: Float, m21: Float, m22: Float, m23: Float,
        m30: Float, m31: Float, m32: Float, m33: Float
    ) {
        this[0, 0] = m00; this[0, 1] = m01; this[0, 2] = m02; this[0, 3] = m03
        this[1, 0] = m10; this[1, 1] = m11; this[1, 2] = m12; this[1, 3] = m13
        this[2, 0] = m20; this[2, 1] = m21; this[2, 2] = m22; this[2, 3] = m23
        this[3, 0] = m30; this[3, 1] = m31; this[3, 2] = m32; this[3, 3] = m33
    }

    internal constructor(inPtr: Pinned<FloatArray>) {
        if (inPtr.get().size != ROWS*COLUMNS) throw InvalidSizeArray("Attempted to use array of size ${inPtr.get().size} for Mat2")
        val arr = inPtr.get()
        this[0, 0] = arr[0]; this[0, 1] = arr[4]; this[0, 2] = arr[8];  this[0, 3] = arr[12]
        this[1, 0] = arr[1]; this[1, 1] = arr[5]; this[1, 2] = arr[9];  this[1, 3] = arr[13]
        this[2, 0] = arr[2]; this[2, 1] = arr[6]; this[2, 2] = arr[10]; this[2, 3] = arr[14]
        this[3, 0] = arr[3]; this[3, 1] = arr[7]; this[3, 2] = arr[11]; this[3, 3] = arr[15]
        inPtr.unpin()
    }
    //endregion

    //region Operator Overloads
    operator fun get(row: Int, column: Int): Float {
        return _mat4.get()[(ROWS * column) + row]
    }
    operator fun set(row: Int, column: Int, value: Float) {
        this[row, column] = value
    }
    operator fun times(rhs: Mat4): Mat4 {
        val dest = zero()
        glm_mat4_mul(this._mat4.ptr(), rhs._mat4.ptr(), dest._mat4.ptr())
        return dest
    }
    operator fun times(rhs: Vec4): Vec4 {
        val dest = Vec4.zero()
        glm_mat4_mulv(this._mat4.ptr(), rhs._vec4.ptr(), dest._vec4.ptr())
        return dest
    }
    operator fun times(rhs: Float): Mat4 {
        val dest = copy(this)
        glm_mat4_scale(dest._mat4.ptr(), rhs)
        return dest
    }
    override operator fun equals(other: Any?): Boolean {
        if (other !is Mat4) return false
        return  this[0, 0] == other[0, 0] && this[1, 0] == other[1, 0] && this[2, 0] == other[2, 0] && this[3, 0] == other[3, 0] &&
                this[0, 1] == other[0, 1] && this[1, 1] == other[1, 1] && this[2, 1] == other[2, 1] && this[3, 1] == other[3, 1] &&
                this[0, 2] == other[0, 2] && this[1, 2] == other[1, 2] && this[2, 2] == other[2, 2] && this[3, 2] == other[3, 2] &&
                this[0, 3] == other[0, 3] && this[1, 3] == other[1, 3] && this[2, 3] == other[2, 3] && this[3, 3] == other[3, 3]
    }
    //endregion

    //region Methods
    fun transpose(): Mat4 {
        val dest = zero()
        glm_mat4_transpose_to(this._mat4.ptr(), dest._mat4.ptr())
        return dest
    }
    fun inverse(): Mat4 {
        val dest = zero()
        glm_mat4_inv(this._mat4.ptr(), dest._mat4.ptr())
        return dest
    }
    fun fastInverse(): Mat4 {
        val dest = zero()
        glm_mat4_inv_fast(this._mat4.ptr(), dest._mat4.ptr())
        return dest
    }
    fun determinant(): Float {
        return glm_mat4_det(this._mat4.ptr())
    }
    fun trace(): Float {
        return glm_mat4_trace(this._mat4.ptr())
    }
    fun trace3(): Float {
        return glm_mat4_trace3(this._mat4.ptr())
    }
    fun swapCol(a: Int, b: Int) {
        glm_mat4_swap_col(this._mat4.ptr(), a, b)
    }
    fun swapRow(a: Int, b: Int) {
        glm_mat4_swap_row(this._mat4.ptr(), a, b)
    }
    fun rmc(row: Vec4, column: Vec4): Float {
        return glm_mat4_rmc(row._vec4.ptr(), this._mat4.ptr(), column._vec4.ptr())
    }
    fun pick3(): Mat3 {
        val dest = Mat3.zero()
        glm_mat4_pick3(this._mat4.ptr(), dest._mat3.ptr())
        return dest
    }
    fun pick3t(): Mat3 {
        val dest = Mat3.zero()
        glm_mat4_pick3t(this._mat4.ptr(), dest._mat3.ptr())
        return dest
    }
    //endregion

    companion object {
        const val ROWS = 4
        const val COLUMNS = 4
        fun identity(): Mat4 {
            val dest = FloatArray(ROWS*COLUMNS).pin()
            glm_mat4_identity(dest.ptr())
            return Mat4(dest)
        }
        fun zero(): Mat4 {
            val dest = FloatArray(ROWS*COLUMNS).pin()
            glm_mat4_zero(dest.ptr())
            return Mat4(dest)
        }
        fun copy(source: Mat4): Mat4 {
            val dest = FloatArray(ROWS*COLUMNS).pin()
            glm_mat4_copy(source._mat4.ptr(), dest.ptr())
            return Mat4(dest)
        }
    }

    private fun finalize() {
        _mat4.unpin()
    }

    override fun hashCode(): Int {
        var result =           this[0, 0].hashCode()
        result = 31 * result + this[1, 0].hashCode()
        result = 31 * result + this[2, 0].hashCode()
        result = 31 * result + this[3, 0].hashCode()
        result = 31 * result + this[0, 1].hashCode()
        result = 31 * result + this[1, 1].hashCode()
        result = 31 * result + this[2, 1].hashCode()
        result = 31 * result + this[3, 1].hashCode()
        result = 31 * result + this[0, 2].hashCode()
        result = 31 * result + this[1, 2].hashCode()
        result = 31 * result + this[2, 2].hashCode()
        result = 31 * result + this[3, 2].hashCode()
        result = 31 * result + this[0, 3].hashCode()
        result = 31 * result + this[1, 3].hashCode()
        result = 31 * result + this[2, 3].hashCode()
        result = 31 * result + this[3, 3].hashCode()
        return result
    }
}
