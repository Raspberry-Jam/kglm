package xyz.raspberryjam.kglm

import cglm.*

/**
 * Generate a perspective pre-projection matrix.
 * @param left Viewport left
 * @param right Viewport right
 * @param bottom Viewport bottom
 * @param top Viewport top
 * @param near Near clipping plane
 * @param far Far clipping plane
 * @return View frustum matrix
 */
fun frustum(left: Float, right: Float, bottom: Float, top: Float, near: Float, far: Float): Mat4 {
    val dest = Mat4.zero()
    glm_frustum(left, right, bottom, top, near, far, dest._mat4.ptr())
    return dest
}

/**
 * Generate a perspective projection matrix.
 * @param fovY Field of view in radians
 * @param aspect width / height
 * @param near Near clipping plane
 * @param far Far clipping plane
 * @return Perspective projection matrix
 */
fun perspective(fovY: Float, aspect: Float, near: Float, far: Float): Mat4 {
    val dest = Mat4.zero()
    glm_perspective(fovY, aspect, near, far, dest._mat4.ptr())
    return dest
}

/**
 * Generate perspective projection matrix using default values, scaled by the window's aspect ratio.
 * @param aspect width / height
 * @return Perspective projection matrix
 */
fun defaultPerspective(aspect: Float): Mat4 {
    val dest = Mat4.zero()
    glm_perspective_default(aspect, dest._mat4.ptr())
    return dest
}

/**
 * Resize perspective matrix by new aspect ratio.
 * @param aspect width / height
 * @param target Existing perspective matrix
 */
fun perspectiveResize(aspect: Float, target: Mat4) {
    glm_perspective_resize(aspect, target._mat4.ptr())
}

/**
 * Generate an orthographic projection matrix.
 * @param left Viewport left
 * @param right Viewport right
 * @param bottom Viewport bottom
 * @param top Viewport top
 * @param near Near clipping plane
 * @param far Far clipping plane
 * @return Orthographic projection matrix
 */
fun orthographic(left: Float, right: Float, bottom: Float, top: Float, near: Float, far: Float): Mat4 {
    val dest = Mat4.zero()
    glm_ortho(left, right, bottom, top, near, far, dest._mat4.ptr())
    return dest
}

/**
 * Generate an orthographic projection matrix using default values, scaled by the window's aspect ratio.
 * @param aspect width / height
 * @return Orthographic projection matrix
 */
fun defaultOrthographic(aspect: Float): Mat4 {
    val dest = Mat4.zero()
    glm_ortho_default(aspect, dest._mat4.ptr())
    return dest
}

/**
 * Create view matrix.
 * @param eye Position of the eye (camera in most cases)
 * @param center Center TODO: better description
 * @param up Direction representing UP
 * @return View matrix
 */
fun lookAt(eye: Vec3, center: Vec3, up: Vec3): Mat4 {
    val dest = Mat4.zero()
    glm_lookat(eye._vec3.ptr(), center._vec3.ptr(), up._vec3.ptr(), dest._mat4.ptr())
    return dest
}

/**
 * Get the field of view of a perspective projection matrix.
 * @param proj Perspective projection matrix
 * @return Field of view in radians
 */
fun getFov(proj: Mat4): Float {
    return glm_persp_fovy(proj._mat4.ptr())
}

/**
 * Get the aspect ratio of a perspective projection matrix.
 * @param proj Perspective projection matrix
 * @return Aspect ratio (width / height)
 */
fun getAspect(proj: Mat4): Float {
    return glm_persp_aspect(proj._mat4.ptr())
}
